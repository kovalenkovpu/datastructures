"use strict";
function Node(item) {
    this.item = item;
    this.link = null;
}

function Stack() {
    this.length = 0;

    var prevNode = null;

    this.LAST = null;

    this.push = function(item) {
        if (this.length === 0) {
            this.LAST = new Node(item);
        } else {
            prevNode = this.LAST;
            this.LAST = new Node(item);
            this.LAST.link = prevNode;
            prevNode = null;
        }

        this.length += 1;
    }

    this.pop = function() {
        if (this.length !== 0) {
            var last = this.LAST;

            prevNode = this.LAST.link;

            this.LAST = prevNode;
            this.length -= 1;

            return last;
        } else {
            return 'Stack is empty!';
        }
    }

    this.count = function() {
        return this.length;
    }

    this.peak = function() {
        if (this.length !== 0) {

            return this.LAST;
        } else {
            return 'Stack is empty!';
        }
    }
}

function checkIfBalanced(string) {
    var stack = new Stack(),
        BRACES = ['(', '{', '[', ')', '}', ']'];

    //checks if parameters are balanced
    //e.g. checkIfBalanced('(', ')') returns true
    function findOpposite(first, second) {
        var index = BRACES.indexOf(first);

        return (second === BRACES[index - 3]) ? true : false;
    }

    for (var i = 0; i < string.length; i++) {
        //if stack is empty and the brace is not closing one
        if ( stack.count === 0 &&
            (string[i] !== BRACES[3] || BRACES[4] || BRACES[5]) ) {

            stack.push(string[i]);
        //if stack is not empty and the brace is opening one
        } else if (string[i] === BRACES[0] ||
                   string[i] === BRACES[1] ||
                   string[i] === BRACES[2]) {
            stack.push(string[i]);
        //if stack is not empty and the brace is closing
        } else {
            if ( findOpposite(string[i], stack.peak().item) ) {
                stack.pop();
            } else {
                stack.push(string[i]);
            }
        }
    }

    return stack.count() === 0 ? 'Expressions is balanced!' : 'Expression isnt balanced!';
}

var template = '({([])})',
    template2 = '[{{}}]',
    template4 = '[{{{()}})}]',
    template5 = '',
    template6 = '{}[](())';

console.log('--------------BALANCED EXPRESSION ({([])}) -------------');
console.log(checkIfBalanced(template));
console.log('--------------BALANCED EXPRESSION [{{}}] ---------------');
console.log(checkIfBalanced(template2));
console.log('--------------NOT BALANCED EXPRESSION [{{{()}})}] ------');
console.log(checkIfBalanced(template4));
console.log('--------------BALANCED EXPRESSION ----------------------');
console.log(checkIfBalanced(template5));
console.log('--------------BALANCED EXPRESSION {}[](()) -------------');
console.log(checkIfBalanced(template6));