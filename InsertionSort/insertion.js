"use strict";

function insertionSort(arr) {
    var N = 0, res = [];

    for (var i = 0; i < arr.length; i++) {
        var el = arr[i];

        N++;

        for (var j = res.length; j >= 0; j--) {
            N++;

            if (el < res[j - 1]) {
                res[j] = res[j - 1];
                res[j - 1] = el;
            } else {
                res[j] = el;
                break;
            }
        }
    }

    console.log(`Array [${res.length}]: [${res}] max operations: ${ Math.pow(arr.length, 2) }, took operations: ${N}`);
}

var arr1 = [1, 2, 3, 5, 1, 2, 1],
    arr2 = [87, 5, 4, 88, 1000, -7],
    arr3 = [0, 1, 2, 3, 4, 5, 6, 7, 8];

insertionSort(arr1);
insertionSort(arr2);
insertionSort(arr3);