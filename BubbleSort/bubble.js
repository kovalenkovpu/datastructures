"use strict";

function bubbleSort(arr) {
    var shifted,
        temp,
        N = 0; // number of iterations through the arr

    for (var j = 0; j < (arr.length - 1); j++) {
        shifted = false;
        N++;

        for (var i = 0; i < (arr.length - 1); i++) {
            N++;

            if (arr[i] > arr[i + 1]) {
                temp = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = temp;
                shifted = true;
            }
        }

        if (shifted === false) break;
    }

    console.log(`Array [${arr.length}]: [${arr}] max operations: ${ Math.pow(arr.length, 2) }, took operations: ${N}`);
}

var arr1 = [1, 2, 3, 5, 1, 2, 1],
    arr2 = [87, 5, 4, 88, 1000, -7],
    arr3 = [0, 1, 2, 3, 4, 5, 6, 7, 8];

bubbleSort(arr1);
bubbleSort(arr2);
bubbleSort(arr3);