# README #

Repository for data structures and algorithms course tasks.

### How to use ###

* It's easier just to pull master branch;
* You can find there several folders - each for one separate task;
* Every folder contains index.html file that can be run in Chrome for example;
* Once it's run press F12 to see the Console tab in Chrome dev tools;
* You'll see there some logs for current task;
* ##CODE itself is in .js file for every folder.##

### Who do I talk to? ###

* Pavel Kovalenkov
* pavel_kovalenkov@epam.com