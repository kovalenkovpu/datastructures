"use strict";
//Node class for creating nodes of a list
function Node(item) {
    this.item = item;
    this.link = null;
}

//linked list class
function LinkedList() {
    this.length = 0;

    var prevNode = null,
        currNode = null,
        nextNode = null;

    this.HEAD = null;

    //method for displaying all nodes
    this.showList = function() {
        var length = this.length,
            node = this.HEAD;

        while (length > 0) {
            console.log('LIST ITEM: ', node);
            node = node.link;
            length -= 1;
        }
    }

    //custom method to retrieve the length
    this._length = function(list) {
        console.log(this.length);
    }

    //method to insert new node into the list
    this.insert = function(item) {
        if (this.length === 0) {
            this.HEAD = new Node(item);
        } else {
            currNode = new Node(item);
            prevNode = this.find(this.length - 1);
            prevNode.link = currNode;
            //now we can remove currNode and prevNode
            //node itself will be stored in memory
            //cause there's the link to that node
            currNode = prevNode = null;
        }

        this.length += 1;
    }

    //method to find the node with the position index
    this.find = function(position) {
        var currNode = this.HEAD;

        if (position >= this.length) return 'Position out of range!';

        for (var k = 0; k < position; k++) {
            currNode = currNode.link;
        }

        return currNode;
    }

    //method to remove the node based on index
    this.remove = function(position) {
        if (position < this.length && position >= 0) {
            if (position === 0) { //if first node
                this.HEAD = this.find(1);
            } else if (position === (this.length - 1)) { //if last node
                this.find(this.length - 2).link = null;
                //garbage collector will automatically destroy last node
                //cause there're no more links to it
            } else {
                prevNode = this.find(position - 1);
                nextNode = this.find(position + 1);
                prevNode.link = nextNode;
                nextNode = prevNode = null;
            }
            this.length -= 1;

        } else if (position) {
            console.log('This element doesnt exist!');
        }
    }
}

//creating the instance
var linkedList = new LinkedList();

console.log('--------------INSERTED 4 NODES--------------------');
linkedList.insert({name: 'Pavel'});
linkedList.insert(2017);
linkedList.insert('Hello!');
linkedList.insert([1,2,3,4]);

console.log('--------------TEST _LENGTH METHOD-----------------');
linkedList._length();

console.log('--------------TEST SHOWLIST METHOD----------------');
linkedList.showList();

console.log('--------------TEST FIND METHOD, POISITION 1-------');
console.log( linkedList.find(1) );

console.log('--------------TEST FIND METHOD, POISITION 4-------');
console.log( linkedList.find(4) );

console.log('--------------REMOVE "2017" NODE------------------');
linkedList.remove(1); //2017
linkedList.showList();

console.log('--------------REMOVE [1,2,3,4] NODE---------------');
linkedList.remove(2); //[1,2,3,4]
linkedList.showList();