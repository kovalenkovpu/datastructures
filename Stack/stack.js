"use strict";
//Node class for creating nodes of a list
function Node(item) {
    this.item = item;
    this.link = null;
}

//Stack class
function Stack() {
    this.length = 0;

    var prevNode = null;

    this.LAST = null;

    //add new element to stack
    this.push = function(item) {
        if (this.length === 0) {
            this.LAST = new Node(item);
        } else {
            prevNode = this.LAST;
            this.LAST = new Node(item);
            this.LAST.link = prevNode;
            prevNode = null;
        }

        this.length += 1;
    }

    //get top element of stack
    this.pop = function() {
        if (this.length !== 0) {
            var last = this.LAST;

            prevNode = this.LAST.link;

            this.LAST = prevNode;
            this.length -= 1;

            return last;
        } else {
            return 'Stack is empty!';
        }
    }

    //amount of items in stack
    this.count = function() {
        return this.length;
    }
}

var stack = new Stack();

console.log('--------------PUSHED 3 ITEMS--------------------');
stack.push({ name: 'Pavel' });
stack.push({ name: 'Alex' });
stack.push('Index.html');

console.log('--------------COUNT-----------------------------');
console.log(stack.count());

console.log('--------------1st POP---------------------------');
console.log(stack.pop());

console.log('--------------2nd POP---------------------------');
console.log(stack.pop());

console.log('--------------3rd POP---------------------------');
console.log(stack.pop());

console.log('--------------4th POP---------------------------');
console.log(stack.pop());

console.log('--------------COUNT-----------------------------');
console.log(stack.count());