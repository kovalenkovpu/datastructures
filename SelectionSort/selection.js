"use strict";

function selectionSort(arr) {
    var N = 0;

    for (var i = 0; i < arr.length - 1; i++) {
        var min = i,
            minEl = arr[i];

        N++;

        for (var j = i + 1; j < arr.length; j++) {
            N++;

            if (arr[j] < arr[min]) {
                min = j;
                minEl = arr[min];
            }
        }

        arr[min] = arr[i];
        arr[i] = minEl;
    }

    console.log(`Array [${arr.length}]: [${arr}] max operations: ${ Math.pow(arr.length, 2) }, took operations: ${N}`);
}

var arr1 = [1, 2, 3, 5, 1, 2, 1],
    arr2 = [87, 5, 4, 88, 1000, -7],
    arr3 = [0, 1, 2, 3, 4, 5, 6, 7, 8];

selectionSort(arr1);
selectionSort(arr2);
selectionSort(arr3);