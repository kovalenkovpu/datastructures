"use strict";
function Node(item) {
    this.item = item;
    this.link = null;
}

function Stack() {
    this.length = 0;

    var prevNode = null;

    this.LAST = null;

    this.push = function(item) {
        if (this.length === 0) {
            this.LAST = new Node(item);
        } else {
            prevNode = this.LAST;
            this.LAST = new Node(item);
            this.LAST.link = prevNode;
            prevNode = null;
        }

        this.length += 1;
    }

    this.pop = function() {
        if (this.length !== 0) {
            var last = this.LAST;

            prevNode = this.LAST.link;

            this.LAST = prevNode;
            this.length -= 1;

            return last;
        } else {
            return 'Stack is empty!';
        }
    }

    this.count = function() {
        return this.length;
    }

    this.peak = function() {
        if (this.length !== 0) {

            return this.LAST;
        } else {
            return 'Stack is empty!';
        }
    }
}

//Queue constructor
function Queue() {
    //use 2 stacks
    this.firstStack = new Stack();
    this.secondStack = new Stack();

    //enqueue method
    this.enqueue = function(item) {
        this.firstStack.push(item);
    }

    //dequeue method
    this.dequeue = function() {
        var firstCount,
            secondCount,
            dequeuedItem;

        firstCount = this.firstStack.count();
        secondCount = null;
        dequeuedItem = null;

        while (firstCount > 0) {
            //pop top item of first stack and push to second stack
            this.secondStack.push(this.firstStack.pop().item);
            firstCount -= 1;
        }

        dequeuedItem = this.secondStack.pop();
        secondCount = this.secondStack.count();

        //push rest of items back to first stack
        while (secondCount > 0) {
            this.firstStack.push(this.secondStack.pop().item);
            secondCount -= 1;
        }

        return dequeuedItem;
    }

    //size of a queue
    this.size = function() {
        return this.firstStack.count();
    }
}

function testQueueSize(queue, toAdd1, toAdd2, toRem1, toRem2, toAdd3) {
    var i = 0,
        corrSize = toAdd1 + toAdd2 + toAdd3 - toRem1 - toRem2;

    for (i = 0; i < toAdd1; i++) {
        queue.enqueue('Test1' + i);
    }

    for (i = 0; i < toAdd2; i++) {
        queue.enqueue('Test2' + i);
    }

    for (i = 0; i < toRem1; i++) {
        queue.dequeue();
    }

    for (i = 0; i < toRem2; i++) {
        queue.dequeue();
    }

    for (i = 0; i < toAdd3; i++) {
        queue.enqueue('Test3' + i);
    }

    console.log('=====correctSize===== ', corrSize);
    console.log('=====testQueueSize===== ', queue.size());
}

var queue = new Queue();

console.log('--------------ENQUEUED 3 ITEMS--------------------');
queue.enqueue({ name: 'Pavel' });
queue.enqueue([1,2,3,4]);
queue.enqueue('Index.html');

console.log('--------------SIZE--------------------------------');
console.log(queue.size());

console.log('--------------1st DEQUEUE expect Obj {}-----------');
console.log(queue.dequeue());
console.log(queue.size());

console.log('--------------2nd DEQUEUE expect Arr []-----------');
console.log(queue.dequeue());
console.log(queue.size());

console.log('--------------3rd DEQUEUE expect String-----------');
console.log(queue.dequeue());

console.log('--------------4th DEQUEUE expect exception--------');
console.log(queue.dequeue());

console.log('--------------SIZE--------------------------------');
console.log(queue.size());

var queue1 = new Queue();

testQueueSize(queue1, 4, 2, 3, 1, 2);

var queue2 = new Queue();

testQueueSize(queue2, 41, 23, 30, 1, 2);